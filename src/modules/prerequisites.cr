module Wince
  extend self

  VERSION    = {{read_file("./shard.yml").split("version: ")[1].split("\n")[0]}}
  {%
    `blueprint-compiler batch-compile ./data/ui/compiled ./data/ui/ ./data/ui/templates/*.blp ./data/ui/*.blp`
  %}
  Gio.register_resource("data/wince.gresource.xml", "data")
end
