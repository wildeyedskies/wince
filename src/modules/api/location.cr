module Wince::Location
  extend self

  @@channel = Channel(Geoclue::Simple).new(1)
  @@client : Geoclue::Simple? = nil
  @@fiber : Fiber = spawn setup_client

  def setup_client
    @@channel.send Geoclue::Simple.new_sync("space.quietfeathers.Wince", Geoclue::AccuracyLevel::Street, nil)
    Fiber.yield
  end

  # returns latlon if available, otherwise empty array
  def find_location
    if !@@fiber.dead?
      return [] of Float64
    end

    if @@client.nil?
      @@client = @@channel.receive
    end

    location = @@client.try do |c|
      [c.location.latitude, c.location.longitude]
    end || [] of Float64
  end
end
