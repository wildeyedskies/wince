module Wince
  @[Gtk::UiTemplate(resource: "/wince/ui/compiled/templates/hourrow.ui",
    children: %w(dayOfWeek hours))]
  class HourRow < Gtk::ListBoxRow
    include Gtk::WidgetTemplate

    @dayOfWeekLabel : Gtk::Label
    @hoursLabel : Gtk::Label

    def initialize(dayOfWeek : Time::DayOfWeek, hours : String)
      super()

      @dayOfWeekLabel = Gtk::Label.cast(template_child("dayOfWeek"))
      @hoursLabel = Gtk::Label.cast(template_child("hours"))

      @dayOfWeekLabel.text = dayOfWeek.to_s
      if hours.empty?
        @hoursLabel.text = "closed"
      else
        @hoursLabel.markup = hours
      end
    end
  end
end