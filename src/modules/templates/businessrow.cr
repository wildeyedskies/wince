module Wince
  @[Gtk::UiTemplate(resource: "/wince/ui/compiled/templates/businessrow.ui",
    children: %w(businessName businessRating businessDistance))]
  class BusinessRow < Gtk::ListBoxRow
    include Gtk::WidgetTemplate

    @name : Gtk::Label
    @rating : Gtk::Label
    @distance : Gtk::Label

    def initialize(name : String, rating : Float32, distance : Float32)
      super()

      @name = Gtk::Label.cast(template_child("businessName"))
      @rating = Gtk::Label.cast(template_child("businessRating"))
      @distance = Gtk::Label.cast(template_child("businessDistance"))

      @name.text = name
      @rating.text = rating.round(2).to_s
      distance_miles = distance / 1609.344
      @distance.text = distance_miles.round(2).to_s + "mi"
    end
  end
end