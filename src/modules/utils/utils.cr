require "time" # yeah me too

module Wince::Utils
  extend self

  @@config_path = Path.home.join("/.config/wince/api_key")
  @@api_key = ""

  def hours_for_day(hours : Yelp::Hours, day : Time::DayOfWeek, seperator : String)
    day_number = day_of_week_to_int(day)

    formatted_hours = hours.open.select { |hour| hour.day == day_number }.map { |hour|
      start_hour = hour.open
      end_hour = hour.close

      "#{start_hour.insert(2, ":")}-#{end_hour.insert(2, ":")}"
    }.join(seperator)
  end

  def day_of_week_to_int(day : Time::DayOfWeek)
    case day
    when Time::DayOfWeek::Monday
      0
    when Time::DayOfWeek::Tuesday
      1
    when Time::DayOfWeek::Wednesday
      2
    when Time::DayOfWeek::Thursday
      3
    when Time::DayOfWeek::Friday
      4
    when Time::DayOfWeek::Saturday
      5
    when Time::DayOfWeek::Sunday
      6
    end
  end

  def format_address(display_address : Array(String))
    display_address.join("\n")
  end

  def api_key_exists?
    File.exists? @@config_path
  end

  def read_api_key
    @@api_key = File.read(@@config_path).strip
  end

  def api_key
    if @@api_key.blank?
      read_api_key
    end

    @@api_key
  end
end
