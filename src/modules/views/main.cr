require "time"

require "../templates/businessrow.cr"
require "../templates/hourrow.cr"
require "../api/location.cr"
require "../api/yelp.cr"
require "../utils/utils.cr"

module Wince
  @@main_window_id = 0_u32
  @@business_rows = [] of BusinessRow
  @@hour_rows = [] of HourRow
  @@business_ids = [] of String
  @@marker = nil

  def activate(app : Adw::Application)
    main_window = APP.window_by_id(@@main_window_id)
    return main_window.present if main_window

    window = Adw::ApplicationWindow.cast(B_UI["mainWindow"])
    window.application = app
    
    @@main_window_id = window.id

    DETAILS_BACK.clicked_signal.connect do
      BUSINESS_LIST.unselect_all
      LEAFLET.visible_child = RESULTS_SCROLL
    end

    SEARCH_BUTTON.clicked_signal.connect do
      handle_search
    end

    GEOLOCATE_BUTTON.clicked_signal.connect do
      handle_geolocate
    end

    BUSINESS_LIST.row_selected_signal.connect do
      handle_business_select
    end

    unless Utils.api_key_exists?
      POWERD_BY_TEXT.visible = false
      CONFIG_NOT_FOUND_TEXT.visible = true
    end

    setup_map
    Location.setup_client

    window.present
  end

  def setup_map
    DETAILS_MAP.map_source = Shumate::MapSourceRegistry
      .new_with_defaults().by_id(Shumate::MAP_SOURCE_OSM_MAPNIK)

    pixbuf = GdkPixbuf::Pixbuf.new_from_resource("/wince/img/marker-icon.png")
    icon = Gtk::Image.new_from_pixbuf(pixbuf)
    @@marker = Shumate::Marker.new
  
    @@marker.try do|m| 
      m.height_request = 64
      m.width_request = 64
      m.child = icon
      marker_layer = Shumate::MarkerLayer.new(DETAILS_MAP.viewport)
      marker_layer.add_marker(m)  
      DETAILS_MAP.add_overlay_layer(marker_layer)
    end
  end

  def handle_geolocate
    latlon = Location.find_location()

    if !latlon.empty?
      LOCATION_ENTRY.text = "#{latlon[0]}, #{latlon[1]}"
    end
  end

  def yelp_response_to_business_ids(businesses : Array(Yelp::Business))
    businesses.map { |b| b.id }
  end

  def yelp_response_to_business_rows(businesses : Array(Yelp::Business))
    businesses.map do |business|
      BusinessRow.new(business.name, business.rating, business.distance)
    end
  end

  def clear_business_rows
    @@business_rows.each { |row| BUSINESS_LIST.remove(row) }
  end

  def clear_hour_rows
    @@hour_rows.each { |row| DETAILS_HOURS_BOX.remove(row) }
  end

  def handle_search
    unless Utils.api_key_exists?
      return
    end

    search = SEARCH_ENTRY.buffer.text
    location = LOCATION_ENTRY.buffer.text

    if search.blank? || location.blank?
      LEAFLET.visible = false
      POWERD_BY_TEXT.visible = true
      return
    end

    LEAFLET.visible = true
    POWERD_BY_TEXT.visible = false

    status_code, response = Yelp.search_businesses(search, location)

    if status_code != 200
      puts "api call error"
      if response.error.is_a? Yelp::Error
        puts response.error.as(Yelp::Error).description
      end
      return #TODO: show a toast here
    end

    # this can technically fail if we get a weird case of a 200 response
    # but a malformed response, but it's probably fine
    businesses = response.businesses.as(Array(Yelp::Business))

    clear_business_rows()
    @@business_ids = yelp_response_to_business_ids(businesses)
    @@business_rows = yelp_response_to_business_rows(businesses)
    @@business_rows.each do |row|
      BUSINESS_LIST.append(row)
    end
  end

  def handle_business_select
    index = @@business_rows.index(BUSINESS_LIST.selected_row) || 0
    id = @@business_ids[index]
    
    status_code, response = Yelp.get_business_info(id)

    if status_code != 200
      puts "api call error"
      if response.error.is_a? Yelp::Error
        puts response.error.as(Yelp::Error).description
      end
      return #TODO: show a toast here
    end


    DETAILS_TITLE.text = response.name || ""

    if response.is_open
      DETAILS_IS_OPEN.markup = "<span foreground=\"green\">open</span>"
    else
      DETAILS_IS_OPEN.markup = "<span foreground=\"red\">closed</span>"
    end

    DETAILS_CURRENT_HOURS.text = Utils.hours_for_day(response.hours[0], Time.local.day_of_week, ", ")
    DETAILS_PRICING.text = response.price || ""
    DETAILS_ADDRESS.markup = Utils.format_address(response.location.display_address)
    DETAILS_PHONE.text = response.display_phone || "no phone number"
    DETAILS_URL.uri = response.url || ""
    
    clear_hour_rows()
    @@hour_rows = format_hours(response.hours[0])
    @@hour_rows.each { |hour_row| DETAILS_HOURS_BOX.append(hour_row) }

    set_map_location(response.coordinates)

    # If we're in the small layout we want to show the back button
    if LEAFLET.folded
      DETAILS_BACK.visible = true
    else
      DETAILS_BACK.visible = false
    end

    DETAILS_BOX.visible = true
    LEAFLET.visible_child = DETAILS_SCROLL
  end

  def set_map_location(coordinates : Yelp::Coordinates)
    viewport = DETAILS_MAP.viewport
    viewport.set_location(coordinates.latitude, coordinates.longitude)
    viewport.zoom_level = 16

    @@marker.try {|m| m.set_location(coordinates.latitude, coordinates.longitude) }
  end

  def format_hours(hours : Yelp::Hours)
    Time::DayOfWeek.values.map do |day|
      hours_for_day = Utils.hours_for_day(hours, day, "\n")
      HourRow.new(day, hours_for_day)
    end
  end

  APP.activate_signal.connect(->activate(Adw::Application))
  exit(APP.run(ARGV))
end
