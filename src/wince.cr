require "libadwaita"
GICrystal.require("Geoclue", "2.0")
GICrystal.require("Shumate", "1.0")

require "./modules/prerequisites.cr"
require "./modules/views/main.cr"

module Wince
  B_UI = Gtk::Builder.new_from_resource("/wince/ui/compiled/main.ui")

  SEARCH_ENTRY = Gtk::Entry.cast(B_UI["searchEntry"])
  LOCATION_ENTRY = Gtk::Entry.cast(B_UI["locationEntry"])
  GEOLOCATE_BUTTON = Gtk::Button.cast(B_UI["geolocateButton"])
  SEARCH_BUTTON = Gtk::Button.cast(B_UI["searchButton"])
  BUSINESS_LIST = Gtk::ListBox.cast(B_UI["searchResults"])
  POWERD_BY_TEXT = Gtk::Label.cast(B_UI["poweredByText"])
  RESULTS_SCROLL = Gtk::ScrolledWindow.cast(B_UI["resultsScroll"])
  DETAILS_SCROLL = Gtk::ScrolledWindow.cast(B_UI["detailsScroll"])
  LEAFLET = Adw::Leaflet.cast(B_UI["leaflet"])
  DETAILS_BOX = Gtk::Box.cast(B_UI["detailsBox"])
  DETAILS_TITLE = Gtk::Label.cast(B_UI["detailsTitle"])
  DETAILS_IS_OPEN = Gtk::Label.cast(B_UI["detailsIsOpen"])
  DETAILS_CURRENT_HOURS = Gtk::Label.cast(B_UI["detailsCurrentHours"])
  DETAILS_PRICING = Gtk::Label.cast(B_UI["detailsPricing"])
  DETAILS_ADDRESS = Gtk::Label.cast(B_UI["detailsAddress"])
  DETAILS_PHONE = Gtk::Label.cast(B_UI["detailsPhone"])
  DETAILS_URL = Gtk::LinkButton.cast(B_UI["detailsUrl"])
  DETAILS_BACK = Gtk::Button.cast(B_UI["detailsBack"])
  DETAILS_HOURS_BOX = Gtk::ListBox.cast(B_UI["detailsHoursBox"])
  DETAILS_MAP = Shumate::SimpleMap.cast(B_UI["detailsMap"])
  CONFIG_NOT_FOUND_TEXT = Gtk::Label.cast(B_UI["configNotFoundText"])

  APP = Adw::Application.new("space.quietfeathers.wince", Gio::ApplicationFlags::None)
end
