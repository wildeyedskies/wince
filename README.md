# Wince

Adaptive GTK4 app for searching local businesses and restaurants. Powered by [Yelp](https://yelp.com).

## Build

### Depenencies

* geoclue
* libshumate
* blueprint-compiler
* libadwaita
* gtk4
* crystal

If you're on arch you can run

```
pacman -S blueprint-compiler crystal shards libadwaita libshumate georclue
```

Then you gotta get an API key from Yelp and put it in a file at `~/.config/wince/api_key`

Then you can run the program with

```
crystal run src/gtktest.cr
```

You can build for production with `make` and install with `make install`

## Credits

Map marker icon from [Paomedia on iconfinder](https://www.iconfinder.com/icons/299087/marker_map_icon)

The [Ultimate GTK4 crystal guide](https://ultimate-gtk4-crystal-guide.geopjr.dev/) without which I would not have been able to figure most of this out.