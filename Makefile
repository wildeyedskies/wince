.PHONY: all install uninstall build
PREFIX ?= /usr

all: bindings build

bindings:
	shards install
	./bin/gi-crystal

build:
	shards build -Dpreview_mt --release --no-debug

install:
	install -D -m 0755 bin/wince $(PREFIX)/bin/wince
	install -D -m 0644 data/space.quietfeathers.wince.desktop $(PREFIX)/share/applications/space.quietfeathers.wince.desktop
	install -D -m 0644 data/img/space.quietfeathers.wince.svg $(PREFIX)/share/icons/hicolor/scalable/apps/space.quietfeathers.wince.svg
	gtk-update-icon-cache /usr/share/icons/hicolor

uninstall:
	rm -f $(PREFIX)/bin/wince
	rm -f $(PREFIX)/share/applications/space.quietfeathers.wince.desktop
	rm -f $(PREFIX)/share/icons/hicolor/scalable/apps/space.quietfeathers.wince.svg
	gtk-update-icon-cache /usr/share/icons/hicolor
